<div class="form-group">
  <label for="frametag">Frametag</label>
  {!! Form::textarea('frametag', null, ['class'=>'form-control redactor', 'id'=>'frametag', 'placeholder'=>'Frametag', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
<div class="form-group">
  <label for="type">Type</label>
  {!! Form::text('type', null, ['class'=>'form-control', 'id'=>'type', 'placeholder'=>'Type', 'required']) !!}
</div>
<div class="form-group">
  <label for="option_id">Option_id</label>
  {!! Form::text('option_id', null, ['class'=>'form-control', 'id'=>'option_id', 'placeholder'=>'Option_id']) !!}
</div>
<div class="form-group">
  <label for="video_id">Video_id</label>
  {!! Form::text('video_id', null, ['class'=>'form-control', 'id'=>'video_id', 'placeholder'=>'Video_id']) !!}
</div>
<div class="form-group clearfix">
	<a href="{{route('adminVideoBanners')}}" class="btn btn-default">Back</a>
	<button type="submit" class="btn btn-primary float-right">
		<i class="fa fa-check" aria-hidden="true"></i>
		Save
	</button>
</div>