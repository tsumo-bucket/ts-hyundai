@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminUserRoles') }}">User Roles</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
</header>
@stop

@section('content')
<div class="row">
  <div class="col-sm-5">
    <div class="caboodle-card">
        <div class="caboodle-card-header">
          <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
        </div>
        <div class="caboodle-card-body">
        {!! Form::open(['route'=>'adminUserRolesStore', 'class'=>'form form-parsley form-create']) !!}
        @include('admin.user_roles.form')
        {!! Form::close() !!}
        <footer>
            <div class="text-right">
                <a href="{{ route('adminUserRoles') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
                <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
            </div>
        </footer>
      </div>
    </div>
  </div>
  <div class="col-sm-7">
    @include('admin.user_roles.permission-form')
  </div>
</div>
@stop

