@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{route('adminDashboard')}}">Dashboard</a></li>
  <li><a href="{{route('adminSamplesTypes')}}">Samples Types</a></li>
  <li class="active">Edit</li>
</ol>
@stop

@section('content')
<div class="col-lg-8">
  <div class="widget">
    <div class="header">
      <div>
        <i class="fa fa-file"></i> Form
      </div>
    </div>
  </div>
  {!! Form::model($data, ['route'=>['adminSamplesTypesUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
  @include('admin.samples_types.form')
  {!! Form::close() !!}
</div>
<div class="col-lg-4">
  <div class="seo-url" data-url="{{route('adminSamplesTypesSeo')}}">
    @include('admin.seo.form')
  </div>
</div>
@stop