@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Page Categories</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{$title}}</h1>
</header>
@stop

@section('content')
<div class="row">
  <div class="col-sm-4 page-categories-section">
    <div class="caboodle-card">
      <div class="caboodle-card-header caboodle-flex caboodle-flex-space-between caboodle-flex-align-center">
        <h4 class="no-margin">Page Categories</h4>
        <a class="btn-transparent btn-sm" href="{{route('adminPageCategoriesCreate')}}" >
            <i class="far fa-plus-circle"></i> Create
        </a>
      </div>
      <div class="filters hide no-padding">
        {!! Form::open(['route'=>'adminPageCategories', 'method' => 'get']) !!}
        <div class="caboodle-form-group caboodle-form-control-connected">
            <label>
                {!! Form::text('name', $keyword, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Search']) !!}
                <button><i class="fa fa-search"></i></button>
            </label>
        </div>
        {!! Form::close() !!}
      </div>
      <div class="caboodle-card-body">
        <div class="table-responsive">
          <div class="elipsis-loader list">
            <div></div>
            <div style="animation-delay: 0.5s"></div>
            <div style="animation-delay: 1s"></div>
          </div>
          {!! Form::open(['method' => 'delete', 'class'=>'form form-delete']) !!}
          <div class="table-body">
              <div class="scrollbar-macosx scroll-content">
                  <table class="caboodle-table">
                      <thead class="">
                          <tr>
                              <th class="caboodle-flex caboodle-flex-align-center" >Name</th>
                              <th width="80"></th>
                          </tr>
                      </thead>
                      <tbody >
                        @if(count(@$data) > 0)
                          @foreach($data as $d)
                          <tr class="category-data" id="{{$d->id}}" style="cursor: pointer;"  >
                            <td class="category-name x-small" id="{{$d->id}}">{{$d->name}}</td>
                            <td class="text-center tag-btn">
                              <a href="{{route('adminPageCategoriesEdit', $d->id)}}" class="btn-edit animated-icon" id="{{$d->id}}" name="{{$d->name}}"><i class="small far fa-edit"></i></a>&nbsp;&nbsp;
                              <a type="submit" data-id="{{$d->id}}" class="btn-delete animated-icon" id="{{$d->id}}"><i class="small far fa-trash"></i></a>
                            </td>
                          </tr>
                          @endforeach
                        @else
                          <tr class="hide">
                              <td colspan="2" class="empty">
                                  No results found
                              </td>
                          </tr>
                        @endif
                      </tbody>
                  </table>
              </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-8 pages-section">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <h4 class="">Pages <span class="sub-header"></span></h4>
      </div>
      <div class="caboodle-card-body">
        <div class="elipsis-loader info">
            <div></div>
            <div style="animation-delay: 0.5s"></div>
            <div style="animation-delay: 1s"></div>
        </div>
        <div class="table-body">
          <div class="scrollbar-macosx scroll-content">
            <table class="caboodle-table">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Slug</th>
                  <th>Published</th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <tr class="no-result hide">
                    <td colspan="5" class="empty">
                        No results found
                    </td>
                </tr>
                <tr class="no-category hide" >
                    <td colspan="5" class="empty select-tag-label">
                        Please select a category
                    </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('copy')
<div class="col-sm-12">
  <div class="widget">
    <div class="header">
      <div>
        <i class="fa fa-table"></i> Table
      </div>
      <div class="right">
        <a class="btn-transparent btn-sm" href="{{route('adminPageCategories')}}"><i class="fa fa-eye"></i> Show All</a>
        <a class="btn-transparent btn-sm" href="{{route('adminPageCategoriesCreate')}}"><i class="fa fa-plus-circle"></i> Create</a>
        <a class="btn-transparent btn-sm" href="#" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-minus-circle"></i> Delete</a>
      </div>
    </div>
    <!-- <div class="filters">
      {!! Form::open(['route'=>'adminPageCategories', 'method' => 'get']) !!}
      <label>
        Search: {!! Form::text('name', $keyword, ['class'=>'form-control input-sm', 'placeholder'=>'']) !!}
        <button><i class="fa fa-search"></i></button>
      </label>
      {!! Form::close() !!}
    </div> -->
    @if (count($data) > 0)
    <div class="table-responsive">
      {!! Form::open(['route'=>'adminPageCategoriesDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
      <table class="table table-bordered table-hover table-striped">
        <thead>
          <tr>
            <th width="30px">
              <label>
                <input type="checkbox" name="delete-all" class="toggle-delete-all">
                <i class="fa fa-square input-unchecked"></i>
                <i class="fa fa-check-square input-checked"></i>
              </label>
            </th>
            <th>Name</th>
            <th>Slug</th>
            <th>Created</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $d)
          <tr>
            <td>
              <label>
                <input type="checkbox" name="ids[]" value="{{$d->id}}">
                <i class="fa fa-square input-unchecked"></i>
                <i class="fa fa-check-square input-checked"></i>
              </label>
            </td>
            <td>{{$d->name}}</td>
            <td>{{$d->slug}}</td>
            <td>
              <?php $created_at = new Carbon($d->created_at); ?>
              {{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
            </td>
            <td width="140px" class="text-center">
              <button type="button" class="btn btn-primary btn-sm" role="button" data-toggle="popover" 
                data-trigger="focus" title="{{$d->name}}" data-placement="left" data-html="true"
                data-content="@include('admin.page_categories.show', ['data' => $d])">
                VIEW
              </button>
              <a href="{{route('adminPageCategoriesEdit', [$d->id])}}" class="btn btn-primary btn-sm">EDIT</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      {!! Form::close() !!}
      @if ($pagination)
      <div class="pagination-links text-right">
        {!! $pagination !!}
      </div>
      @endif
    </div>
    @else
    <div class="empty text-center">
      No results found
    </div>
    @endif
  </div>
</div>
@stop

@section('added-scripts')

  <script id="page-table-template" type="text/x-handlebars-template">
    <tr class="page-data">
      <td>@{{ name }}</td>
      <td>@{{ slug }}</td>
      <td>@{{ published }}</td>
      <td width="50px" class="text-center">
        <a
            href="@{{ edit_path }}"
            class="mdc-icon-toggle animated-icon"
            role="button"
            aria-pressed="false"
            permission-action="edit"
            data-toggle="tooltip"
            title="Manage page">
            <i class="far fa-edit" aria-hidden="true"></i>
        </a>
      </td>
    </tr>
  </script>


  <script>
    $(document).ready(function(){
      var itemToDelete = null;
      var selectedCategory = "";
      var selectedCategoryClass = null;
      var hasPages = null ;
      var PageItemHTML = Handlebars.compile($("#page-table-template").html());

      $('.table-body').css('display', 'none');

      setTimeout(() => {

        $('.elipsis-loader').css('display', 'none');
        $('.table-body').css('display', 'block');

        if(selectedCategory == ''){
          $('.pages-section table thead').addClass('hide');
          $('.no-category').removeClass('hide');
        }
      }, 2000);

      $('tr.category-data .btn-delete').click(function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        selectedCategory = {
          ids: id
        };
        getPages(id);
        setTimeout(() => {
          if(hasPages){
          swal({
            title: "Cannot Delete Category",
            text:"There are pages referenced to this category",
            icon: "error",
            type: "error",
            confirmButtonText: "OK",
            closeOnConfirm: true,
            showCancelButton: false,
            dangerMode: true
          },
          function(){
            console.log('cancelled');
          });
        }
        else{
          swal({
              title: "Confirm Action",
              text:"Are you sure you want to delete the selected item?",
              icon: "warning",
              type: "warning",
              confirmButtonText: "OK",
              closeOnConfirm: true,
              showCancelButton: true,
              dangerMode: true
            },
            function(){
              $.ajax({
                type: 'DELETE',
                url: "{{ route('adminPageCategoriesDestroy') }}",
                data: {
                  input: selectedCategory
                },
                dataType: 'json',
                success : function(data) {
                  var title = data.notifTitle;
                  var message = (_.isUndefined(data.notifMessage) ? '' : data.notifMessage);
                  var status = (_.isUndefined(data.notifStatus) ? 'success' : data.notifStatus);
                  
                  if (status != '' && (title != '' || message != '')) {
                    toastr.clear();
                    showNotifyToaster(status, title, message);
                  }

                  if (data.resetForm) {
                    resetForm(formElem);
                  }

                  if (! _.isUndefined(data.redirect)) {
                    setTimeout(function() {
                      window.location = data.redirect;
                    }, 1500);
                  } else {
                            showLoader(false);
                        }
                  try {
                            sumoFormSuccessHandler(data, formElem);
                  } catch(e) {}
                },
                error : function(data, text, error) {
                  showLoader(false);
                  var message = '';
                  _.each(data.responseJSON, function(val) {
                    message += val + ' ';
                  });
                  toastr.clear();
                  showNotifyToaster('error', 'Error saving.', message);
                }
              });
          });
        }
        }, 500);
      });

      $('.category-name').click(function(){
        var id = $(this).attr('id');
        var temp = {
          id : id
        };
        selectedCategory = temp;
        if(selectedCategoryClass == $(this) || selectedCategoryClass == null ){
          selectedCategoryClass = $(this).closest('.category-data');
          $(this).closest('.category-data').addClass('selected');
        }
        else{
          $(selectedCategoryClass).closest('.category-data').removeClass('selected');
          selectedCategoryClass = $(this).closest('.category-data');
          $(this).closest('.category-data').addClass('selected');
        }
        getPages(id);
      });

      function clearPageTableBody(){
        $('.pages-section table thead').addClass('hide');
        $('.no-result, .no-category').addClass('hide');
        $('.pages-section table tbody').children().not('.no-result, .no-category').remove();
        $('.pages-section .elipsis-loader').css('display', 'flex');
      }

      function getPages(id){
        $.ajax({
          type: 'GET',
          url: "{{ route('adminPageByCategory') }}",
          data: {
            category: selectedCategory
          },
          dataType: 'json',
          beforeSend: function(){
            clearPageTableBody();
          },
          success: function(data){
            console.log(data);
            $('.pages-section .elipsis-loader').css('display', 'none');
            $('.pages-section table thead').removeClass('hide');
            if(data.length > 0){
              hasPages = true;
              _.each(data, function(page){
                var html = PageItemHTML({
                  id: page.id,
                  name: page.name,
                  slug: page.slug,
                  published: page.published,
                  edit_path: page.edit_path
                });
                $('.no-category').addClass('hide');
                $('.no-result').addClass('hide');
                $('.pages-section table tbody').append(html);
              });
              
            }
            else{
              hasPages = false;
              $('.no-result').removeClass('hide');
              $('.pages-section table thead').addClass('hide');
              $('.no-category').addClass('hide');
            }
          }
        })
      };

      
    });
  </script>
@stop