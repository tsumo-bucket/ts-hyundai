<div class="caboodle-card">
	<div class="caboodle-card-header pad-top-15 pad-bottom-15">
		<div class="filters no-padding">
			<div class="flex align-center caboodle-form-control-connected">
				<h4 class="no-margin flex-1">
					Form Controls
				</h4>
				<a href="#" class="add-form-control-toggle btn-transparent btn-sm flex-1 text-right">
					<i class="far fa-plus-circle"></i> Add
				</a>
			</div>
		</div>
	</div>
	<div class="caboodle-card-body">
		<a href="#" class="empty-form-controls add-form-control-toggle {{$data->controls->count() > 0 ? 'hide' : ''}}">
			<div class="empty-message" role="button">
				Add form controls here...
			</div>
		</a>
		<div class="form-controls-container form-controls-sortable">
			@foreach($data->controls as $key => $control)
			<div class="flex margin-bottom form-control-wrapper" sortable-id="{{ $control->id }}">
				<div class="flex-auto form-input-icon-control pad-right no-left-padding">
					<a href="#">
						<i class="far fa-ellipsis-v hover-scale" role="button" data-toggle="tooltip" title="Drag and Reorder"></i>
					</a>
				</div>
				<div class="flex-1">
					<div class="caboodle-form-group">
						<input type="hidden" name="control[]" value="{{ $control->id }}">
						<label for="name[]" class="less-margin x-small">Name</label>
						{!! Form::text('name[]', @$control->name, ['class'=>'form-control', 'placeholder'=>'', 'autocomplete'=>'off', 'required']) !!}
					</div>
				</div>
				<div class="flex-1">
					<div class="caboodle-form-group">
						<label for="label[]" class="less-margin x-small">Label</label>
						{!! Form::text('label[]', @$control->label, ['class'=>'form-control', 'placeholder'=>'', 'autocomplete'=>'off', 'required']) !!}
					</div>
				</div>
				<div class="flex-1">
					<div class="caboodle-form-group margin-bottom">
						<label for="type[]" class="less-margin x-small">Form Control Type</label>
						<select name="type[]" class="form-control" autocomplete="off" required >
							<option value="text" @if($control->type == 'text') selected @endif>Text</option>
							<option value="number" @if($control->type == 'number') selected @endif>Number</option>
							<option value="checkbox" @if($control->type == 'checkbox') selected @endif>Checkbox</option>
							<option value="textarea" @if($control->type == 'textarea') selected @endif>Textarea</option>
							<option value="asset" @if($control->type == 'asset') selected @endif>Asset</option>
							<option value="select" @if($control->type == 'select') selected @endif>Select</option>
							<option value="date" @if($control->type == 'date') selected @endif>Date</option>
							<option value="time" @if($control->type == 'time') selected @endif>Time</option>
							<option value="date_time" @if($control->type == 'date_time') selected @endif>Date Time</option>
							<option value="products" @if($control->type == 'products') selected @endif>Products Selector</option>
						</select>
					</div>
					<div class="caboodle-form-group caboodle-form-group-code-editor @if($control->type !== 'select') hide @endif">
						<label for="options_json[]" class="less-margin xx-small">Options in JSON</label>
						<textarea readonly name="options_json[]" class="form-control" role="button">{{ @$control->options_json }}</textarea>
					</div>
				</div>
				<div class="flex-auto margin-5">
					<div class="caboodle-form-group">
						<label for="required[]" class="less-margin x-small" value="{{ $control->id }}">Required</label>
						<div class="mdc-form-field caboodle-flex caboodle-flex-center" width="40px">
							<div class="mdc-checkbox no-margin">
								<input type="checkbox" name="required[]" class="mdc-checkbox__native-control"
								 {{ @$control->required > 0 ? "checked" : "" }} value="{{ $control->id }}"/>
								<div class="mdc-checkbox__background">
									<svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
										<path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
									</svg>
									<div class="mdc-checkbox__mixedmark"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="flex-auto form-input-icon-control">
					<a href="#" class="edit-control" data-id="@{{ id }}">
						<i class="far fa-edit hover-scale" role="button" data-toggle="tooltip" title="Edit"></i>
					</a>
				</div>
				<div class="flex-auto form-input-icon-control">
					<a href="#" class="delete-control" data-id="{{ $control->id }}">
						<i class="far fa-trash hover-scale color-red" role="button" data-toggle="tooltip" title="Delete"></i>
					</a>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
