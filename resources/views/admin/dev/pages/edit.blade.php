@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminPages') }}">Pages</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    @if(!@$data->seo)
        <div class="header-actions">
            <a href="{{ route('adminPages') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
            <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
        </div>
    @endif
</header>
@stop

@if(!@$data->seo)
    @section('footer')
    <footer>
        <div class="text-right">
            <a href="{{ route('adminPages') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
            <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
        </div>
    </footer>
    @stop
@endif

<?php
// @section('content')
//     <div class="row">
//         @if(@$data->seo)
//         <div class="col-sm-7">
//             <div class="caboodle-card">
//                 <div class="caboodle-card-header">
//                     <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
//                 </div>
//                 <div class="caboodle-card-body">
//                     {!! Form::model($data, ['route'=>['adminPagesUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
//                     @include('admin.dev.pages.form')
//                     {!! Form::close() !!}
//                     <footer>
//                         <div class="text-right">
//                             <a href="{{ route('adminPages') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
//                             <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
//                         </div>
//                     </footer>
//                 </div>
//             </div>
//         </div>
//         <!-- <div class="col-sm-5">
//             <div class="caboodle-card">
// 				<div class="caboodle-card-header pad-top-15 pad-bottom-15">
// 					<div class="filters no-padding">
// 						<div class="flex align-center caboodle-form-control-connected">
// 							<h4 class="no-margin flex-1">
// 								Page Contentsss
// 							</h4>
//                             <a href="{{ route('adminPageContents') }}" class="btn-transparent btn-sm flex-1 text-right">
// 								<i class="far fa-plus-circle"></i> Add
// 							</a>
//                         </div>
//                     </div>
//                 </div>
//             </div>
// 		</div> -->
//         @else
//         <div class="col-sm-12">
//             <div class="caboodle-card">
//                 <div class="caboodle-card-header">
//                     <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
//                 </div>
//                 <div class="caboodle-card-body">
//                 {!! Form::model($data, ['route'=>['adminPagesUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
//                 @include('admin.dev.pages.form')
//                 {!! Form::close() !!}
//                 </div>
//             </div>
//         </div>
//         @endif
//     </div>
//     <div class="row">
//         <div class="col-sm-8">
// 			<div class="caboodle-card">
//                 <div class="caboodle-card-header">
//                     <h4 class="no-margin"><i class="far fa-globe"></i> SEO</h4>
//                 </div>
// 				<div class="caboodle-card-body">
// 					<div class="seo-url" data-url="{{route('adminPagesSeo')}}">
// 						@include('admin.seo.form')
// 					</div>
// 				</div>
// 			</div>
// 		</div>
//     </div>
// @stop
?>
@section('footer')
<footer>
	<div class="text-right">
		<a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{route('adminPages')}}">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save Changes</button>
	</div>
</footer>
@stop

@section('content')
{!! Form::model($data, ['route'=>['adminPagesUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit form-clear']) !!}
	@include('admin.dev.pages.form')
{!! Form::close() !!}
@stop
