<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id');
            $table->integer('reference_id');
            $table->string('reference_type');
            $table->string('name');
            $table->string('slug');
            $table->string('title');
            $table->text('description');
            $table->integer('bg_image');
            $table->enum('published', ['draft', 'published'])->default('draft');
            $table->integer('order');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['page_id']);
            $table->tinyInteger('enabled')->default(1);
			$table->tinyInteger('editable')->default(1);
			$table->tinyInteger('allow_add_items')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_contents');
    }
}
